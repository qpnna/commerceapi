const sql = require("./db.js");

// Contructeur
const Utilisateur = function(Utilisateur) {
  this.nom = Utilisateur.nom;
  this.prenom = Utilisateur.prenom;
  this.email = Utilisateur.email;
  this.mdp = Utilisateur.mdp;
  this.adresse = Utilisateur.adresse;
  this.ville = Utilisateur.ville;
  this.cp = Utilisateur.cp
};

Utilisateur.create = (newUtilisateur, result) => {
  sql.query("INSERT INTO Utilisateurs SET ?", newUtilisateur, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("Utilisateur créé : ", { id: res.insertId, ...newUtilisateur });
    result(null, { id: res.insertId, ...newUtilisateur });
  });
};

Utilisateur.findById = (UtilisateurId, result) => {
  sql.query(`SELECT * FROM Utilisateurs WHERE id = ${UtilisateurId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found Utilisateur: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Utilisateur with the id
    result({ kind: "not_found" }, null);
  });
};

Utilisateur.findByEmailAndPwd = (UtilisateurEmail, UtilisateurPwd, result) => {
  sql.query(`SELECT * FROM Utilisateurs WHERE email = '${UtilisateurEmail}' AND mdp = '${UtilisateurPwd}'`, (err, res) => {
    console.log(err, res)
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length === 1) {
      console.log("found Utilisateur: ", res);
      result(null, res[0]);
      return;
    }

    // not found Utilisateur with the id
    result({ kind: "not_found" }, null);
  });
};

Utilisateur.getAll = result => {
  sql.query("SELECT * FROM Utilisateurs", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Utilisateurs: ", res);
    result(null, res);
  });
};

Utilisateur.updateById = (id, Utilisateur, result) => {
  sql.query(
    "UPDATE Utilisateurs SET nom = ?, prenom = ?, email = ?, mdp = ?, adresse = ?, ville = ?, cp = ? WHERE id = ?",
    [Utilisateur.nom, Utilisateur.prenom, Utilisateur.email, Utilisateur.mdp, Utilisateur.adresse, Utilisateur.ville, Utilisateur.cp, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Utilisateur with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated Utilisateur: ", { id: id, ...Utilisateur });
      result(null, { id: id, ...Utilisateur });
    }
  );
};

Utilisateur.remove = (id, result) => {
  sql.query("DELETE FROM Utilisateurs WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Utilisateur with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Utilisateur with id: ", id);
    result(null, res);
  });
};

Utilisateur.removeAll = result => {
  sql.query("DELETE FROM Utilisateurs", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} Utilisateurs`);
    result(null, res);
  });
};

module.exports = Utilisateur;
