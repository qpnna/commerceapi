const sql = require("./db.js");

// Contructeur
const Produit = function(Produit) {
  this.titre = Produit.titre;
  this.description = Produit.description;
  this.image = Produit.image;
  this.prix = Produit.prix;
};

Produit.create = (newProduit, result) => {
    sql.query("INSERT INTO Produits SET ?", newProduit, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("Produit créé : ", { id: res.insertId, ...newProduit });
      result(null, { id: res.insertId, ...newProduit });
    });
  };

Produit.findById = (ProduitId, result) => {
    sql.query(`SELECT * FROM Produits WHERE id = ${ProduitId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found Produit: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      result({ kind: "not_found" }, null);
    });
  };
  
  Produit.getAll = result => {
    sql.query("SELECT * FROM Produits", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Produits : ", res);
      result(null, res);
    });
  };
  
  Produit.updateById = (id, Produit, result) => {
    sql.query(
      "UPDATE Produits SET titre = ?, description = ?, image = ?, prix = ? WHERE id = ?",
      [Produit.titre, Produit.description, Produit.image, Produit.prix, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated Produit: ", { id: id, ...Produit });
        result(null, { id: id, ...Produit });
      }
    );
  };
  
  Produit.remove = (id, result) => {
    sql.query("DELETE FROM Produits WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted Produit with id: ", id);
      result(null, res);
    });
  };
  
  Produit.removeAll = result => {
    sql.query("DELETE FROM Produits", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} Produits`);
      result(null, res);
    });
  };
  
  module.exports = Produit;
  