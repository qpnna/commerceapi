module.exports = app => {
    const produits = require("../controllers/produits.controller.js");
  
    // Créer un produit
    app.post("/produits", produits.create);
  
    // Obtenir tous les produits
    app.get("/produits", produits.findAll);
  
    // Obtenir un produits à partir d'un ID
    app.get("/produits/:produitId", produits.findOne);
  
    // Mettre un jour un produit à partir d'un ID
    app.put("/produits/:produitId", produits.update);
  
    // Supprimer un produit à partir d'un ID
    app.delete("/produits/:produitId", produits.delete);
  
    // Supprimer tout les produits
    app.delete("/produits", produits.deleteAll);
  };
  