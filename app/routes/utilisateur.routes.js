module.exports = app => {
  const utilisateurs = require("../controllers/utilisateur.controller.js");
  const login = require("../controllers/login.controller.js");

  // Créer un utilisateur
  app.post("/utilisateurs", utilisateurs.create);

  // Obtenir tous les utilisateurs
  app.get("/utilisateurs", utilisateurs.findAll);

  // Obtenir un utilisateur à partir d'un ID
  app.get("/utilisateurs/:utilisateurId", utilisateurs.findOne);

  // Mettre un jour un utilisateur à partir d'un ID
  app.put("/utilisateurs/:utilisateurId", utilisateurs.update);

  // Supprimer un utilisateur à partir d'un ID
  app.delete("/utilisateurs/:utilisateurId", utilisateurs.delete);

  // Supprimer tout les utilisateurs
  app.delete("/utilisateurs", utilisateurs.deleteAll);

  // Se connecter
  app.post("/login", login.login);
};
