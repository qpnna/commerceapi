const Utilisateur = require("../models/utilisateur.model.js");

exports.login = (req, res) => {
  Utilisateur.findByEmailAndPwd(req.body.username, req.body.password, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Identifiants incorrectes !`
        });
      }
    } else res.send(data);
  });
};
