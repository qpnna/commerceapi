const Produit = require("../models/produit.model.js");

// Créer et enregistrer un produit
exports.create = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Le contenu ne peut être vide."
      });
    }
  
    const produit = new Produit({
      titre: req.body.titre,
      description: req.body.description,
      image: req.body.image,
      prix: req.body.prix,
    });
  
    Produit.create(produit, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Une erreur est survenue."
        });
      else res.send(data);
    });
  };
  
  // Obtenir tous les produits.
  exports.findAll = (req, res) => {
    Produit.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Une erreur est survenue."
        });
      else res.send(data);
    });
  };
  
  // Récupérer un produit avec un ID
  exports.findOne = (req, res) => {
    Produit.findById(req.params.produitId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Produit non trouvé pour l'ID ${req.params.produitId}.`
          });
        } else {
          res.status(500).send({
            message: "Une erreur est survenue pour la récupération de l'utilisateur avec l'ID " + req.params.produitId
          });
        }
      } else res.send(data);
    });
  };
  
  // Mettre à jour un produit
  exports.update = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Le contenu ne peut être vide."
      });
    }
  
    Produit.updateById(
      req.params.produitId,
      new Produit(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Produit non trouvé pour l'ID ${req.params.produitId}.`
            });
          } else {
            res.status(500).send({
              message: "Une erreur est survenue pour la mise à jour du produit avec l'ID " + req.params.produitId
            });
          }
        } else res.send(data);
      }
    );
  };
  
  // Supprimer un produit
  exports.delete = (req, res) => {
    Produit.remove(req.params.produitId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Produit non trouvé pour l'ID ${req.params.produitId}.`
          });
        } else {
          res.status(500).send({
            message: "Une erreur est survenue pour la suppression du produit avec l'ID " + req.params.produitId
          });
        }
      } else res.send({ message: `Suppression effectuée.` });
    });
  };
  
  // Supprimer tous les produits
  exports.deleteAll = (req, res) => {
    Produit.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Une erreur est survenue lors de la suppression de tous les produits."
        });
      else res.send({ message: `Tous les produits ont été supprimés` });
    });
  };
  