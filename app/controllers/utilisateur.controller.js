const Utilisateur = require("../models/utilisateur.model.js");

// Créer et enregistrer un utilisateur
exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Le contenu ne peut être vide."
    });
  }

  const utilisateur = new Utilisateur({
    nom: req.body.nom,
    prenom: req.body.prenom,
    email: req.body.email,
    mdp: req.body.mdp,
    adresse: req.body.adresse,
    ville: req.body.ville,
    cp: req.body.cp
  });

  Utilisateur.create(utilisateur, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Une erreur est survenue."
      });
    else res.send(data);
  });
};

// Obtenir tous les utilisateurs.
exports.findAll = (req, res) => {
  Utilisateur.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Une erreur est survenue."
      });
    else res.send(data);
  });
};

// Récupérer un utilisateur avec un ID
exports.findOne = (req, res) => {
  Utilisateur.findById(req.params.utilisateurId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Utilisateur non trouvé pour l'ID ${req.params.utilisateurId}.`
        });
      } else {
        res.status(500).send({
          message: "Une erreur est survenue pour la récupération de l'utilisateur avec l'ID " + req.params.utilisateurId
        });
      }
    } else res.send(data);
  });
};

// Mettre à jour un utilisateur
exports.update = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Le contenu ne peut être vide."
    });
  }

  Utilisateur.updateById(
    req.params.utilisateurId,
    new Utilisateur(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Utilisateur non trouvé pour l'ID ${req.params.utilisateurId}.`
          });
        } else {
          res.status(500).send({
            message: "Une erreur est survenue pour la mise à jour de l'utilisateur avec l'ID " + req.params.utilisateurId
          });
        }
      } else res.send(data);
    }
  );
};

// Supprimer un utilisateur
exports.delete = (req, res) => {
  Utilisateur.remove(req.params.utilisateurId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Utilisateur non trouvé pour l'ID ${req.params.utilisateurId}.`
        });
      } else {
        res.status(500).send({
          message: "Une erreur est survenue pour la suppression de l'utilisateur avec l'ID " + req.params.utilisateurId
        });
      }
    } else res.send({ message: `Suppression effectuée.` });
  });
};

// Supprimer tous les utilisateurs
exports.deleteAll = (req, res) => {
  Utilisateur.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Une erreur est survenue lors de la suppression de tous les utilisateurs."
      });
    else res.send({ message: `Tous les utilisateurs ont été supprimés` });
  });
};
